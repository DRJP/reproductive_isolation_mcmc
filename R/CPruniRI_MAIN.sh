#!/bin/sh

## The following takes the qsub job number and prints it to the qsub output file in ~/
## Note. This works in BASH shells but not C shells.
## By default migale uses a C shell
## So use a -S /bin/sh argument when using qsub

qsubID=$JOB_ID
iChain=$1

echo "From CPruni_MAIN.sh"
echo "qsub process number is" $qsubID
echo "iChain is" $iChain

## Now call the R script
echo "Calling R script CPruni_MAIN.R"
echo 'source("/projet/extern/save/dpleydell/pruni_version_control/R/CPruniRI_MAIN.R");' | R --vanilla --quiet --args $iChain $qsubID 

